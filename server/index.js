const express = require('express');
const bodyParser = require('body-parser');

const port = 3001;
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const messageRouter = require('./routers/messages.router');
const userRouter = require('./routers/users.router');


app.use((req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
// Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    res.setHeader('Access-Control-Allow-Headers','*');
    next();
});

app.use('/messages', messageRouter);
app.use('/users', userRouter);

app.listen(port, () => console.log("Server start on port " + port));