const findInArray = (arr, value, key = '') => {
    for (let i = 0; i < arr.length; i++) {
        if (key && arr[i][key] == value)
            return i;
        if (!key && arr[i] == value)
            return i;
    }
    return -1;
};
module.exports = findInArray;