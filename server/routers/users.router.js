const jwt = require('jsonwebtoken');

const express = require('express');
const router = express.Router();

const usersRep = require('../repository/users.repository');

router.get('/', (req, res) => {
   usersRep.getAllWithoutPassword()
       .then((data) => res.json(data))
       .catch(e => {
           console.log(e);
           res.status(500).send();
       })
});

router.get('/:id', (req, res) => {
    usersRep.getWithoutPassword(req.params.id)
        .then((data) => res.json(data))
        .catch(e => {
            console.log(e);
            res.status(500).send();
        })
});

router.post('/auth', (req, res) => {
    usersRep.checkUser(req.body.user, req.body.password)
        .then(data => {
            if (data) {
                const token = jwt.sign(data, 'secret', {});
                res.json({...data, token});
            }
            res.status(403).send();
        })
        .catch(() => res.status(500).send())
});

router.post('/add', (req, res) => {
    usersRep.addNew(req.body.user)
        .then(() => res.status(200).send())
        .catch(() => res.status(500).send());
});

router.put('/', (req, res) =>{
    usersRep.changeUser(req.body.user)
        .then(() => res.status(200).send())
        .catch(() => res.status(500).send());
});

router.delete('/:id', (req, res) => {
    usersRep.deleteUser(req.params.id)
        .then(() => res.status(200).send())
        .catch((e) => {
            console.log(e);
            res.status(500).send()
        });
});

module.exports = router;