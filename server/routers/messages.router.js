const express = require('express');
const router = express.Router();

const messagesRep = require('../repository/messages.repository');
const findInArray = require('../helpers/findInArray.helper');

router.get('/', (req, res) => {
    messagesRep.getAll()
        .then(file => res.json(file))
        .catch(e => res.send(e));
});

router.get('/:id', (req, res) => {
    // todo if no id
    messagesRep.getAll()
        .then(file => {
            const index = findInArray(file, req.params.id, 'id');
            res.json(file[index]);
        })
        .catch(e => res.send(e));
});

router.post('/', (req, res) =>{
    messagesRep.addNew(req.body.message)
        .then(() => res.status(200).send())
        .catch((e) => {
            console.log(e);
            res.status(500).send()
        });
});

router.put('/', (req, res) => {
    messagesRep.changeMessage(req.body.id, req.body.value)
        .then(() => res.status(200).send())
        .catch(() => res.status(500).send());
});

router.delete('/:id', (req, res) => {
    messagesRep.deleteMessage(req.params.id)
        .then(() => res.status(200).send())
        .catch(() => res.status(500).send());
});

router.put('/like', (req, res) => {
    messagesRep.updateLike(req.body.id, req.body.user)
        .then(() => res.status(200).send())
        .catch((e) => {
            console.log(e);
            res.status(500).send()
        });
});


module.exports = router;