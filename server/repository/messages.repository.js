const fs = require("fs");
const dirMessages = '../data/messages.json';
const findInArray = require('../helpers/findInArray.helper');

const getAll = () => {
    return new Promise((resolve, rejective) => {
        fs.readFile(dirMessages, 'utf-8', (err, data) => {
            if (err) return rejective(err);
            resolve(JSON.parse(data));
        })
    });
};

const addNew = (message) => {
    console.log('add mess');
    return new Promise((res, rej) => {
        getAll()
            .then(messages => {
                messages.push(message);
                fs.writeFile(dirMessages, JSON.stringify(messages), (err) => {
                    console.log(err);
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};

const changeMessage = (id, value) => {
    return new Promise((res, rej) => {
        getAll()
            .then(messages => {
                const index = findInArray(messages, id, 'id');
                    messages[index].message = value;

                fs.writeFile(dirMessages, JSON.stringify(messages), (err) => {
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};

const deleteMessage = (id) => {
    return new Promise((res, rej) => {
        getAll()
            .then(messages => {
                const index = findInArray(messages, id, 'id');
                if (index === -1)
                    return res();
                messages.splice(index, 1);
                fs.writeFile(dirMessages, JSON.stringify(messages), (err) => {
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};

const updateLike = (id, user) => {
    return new Promise((res, rej) => {
        getAll()
            .then(messages => {
                const index = findInArray(messages, id, 'id');
                if (index === -1)
                    return res();
                let reactions = messages[index].reactions;
                if (reactions) {
                    let j = findInArray(reactions, user);
                    console.log(reactions, user);
                    if (j === -1)
                        reactions = [...reactions, user];
                    else
                        reactions.splice(j, 1);
                } else
                    reactions = [user];
                messages[index].reactions = reactions;
                fs.writeFile(dirMessages, JSON.stringify(messages), (err) => {
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};


module.exports = {
    getAll,
    addNew,
    changeMessage,
    deleteMessage,
    updateLike
};