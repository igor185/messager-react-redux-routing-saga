const findInArray = require('../helpers/findInArray.helper');

const fs = require("fs");
const dirUsers = '../data/users.json';


const getAll = () => {
    return new Promise((resolve, rejective) => {
        fs.readFile(dirUsers, 'utf-8', (err, data) => {
            if (err) return rejective(err);
            resolve(JSON.parse(data));
        })
    });
};

const getAllWithoutPassword = async () => {
    const users = await getAll();
    return  users.map( user => {
        user.password = null;
        return user;
    })
};



const getWithoutPassword = async ( id ) => {
    const users = await getAll();
    const index = findInArray(users, id, 'id');
    if(index === -1)
        throw new Error();
    return users[index];
};

const checkUser = async (user, password) => {
    const users = await getAll();
    const index = findInArray(users, user, 'user');
    if(index === -1)
        return false;
    if(users[index].password === password)
        return {user, avatar: users[index].avatar, isAdmin: users[index].isAdmin};
    return false;
};

const addNew = (user) => {
    return new Promise((res, rej) => {
        getAll()
            .then(users => {
                users.push(user);
                fs.writeFile(dirUsers, JSON.stringify(users), (err) => {
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};

const changeUser= (user) => {
    return new Promise((res, rej) => {
        getAll()
            .then(users => {
                const index = findInArray(users, user.id, 'id');
                if (index === -1)
                    users.push(user);
                else
                    users[index] = user;

                fs.writeFile(dirUsers, JSON.stringify(users), (err) => {
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};


const deleteUser = (id) => {
    return new Promise((res, rej) => {
        getAll()
            .then(users => {
                const index = findInArray(users, id, 'id');
                if (index === -1)
                    return res();
                users.splice(index, 1);
                fs.writeFile(dirUsers, JSON.stringify(users), (err) => {
                    if (err) return rej(err);
                    res();
                });
            })
            .catch(e => rej(e));
    });
};


module.exports = {
    checkUser,
    addNew,
    changeUser,
    deleteUser,
    getAllWithoutPassword,
    getWithoutPassword
};