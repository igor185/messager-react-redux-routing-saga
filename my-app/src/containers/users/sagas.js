import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_USER, UPDATE_USER, DELETE_USER, FETCH_USERS } from "./actionTypes";
import {DISABLE_ERROR, ERROR_WITH_MESSAGE, HID_SPINNER, SHOW_SPINNER} from "../Login/actionTypes";
import delay from "../../helpers/delay";

export function* fetchUsers() {
    yield put({type: SHOW_SPINNER});
    try {
        const users = yield call(axios.get, `http://localhost:3001/users`);
        yield put({type: HID_SPINNER});
        yield put({ type: 'FETCH_USERS_SUCCESS', payload: { users: users.data } })
    } catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}

function* watchFetchUsers() {
    yield takeEvery(FETCH_USERS, fetchUsers)
}

export function* addUser(action) {
    yield put({type: SHOW_SPINNER});
    const newUser = { ...action.payload.data, id: action.payload.id };

    try {
        yield call(axios.post, `http://localhost:3001/users/add`, {user: {...newUser}});
        yield put({ type: FETCH_USERS });
        yield put({type: HID_SPINNER});
    } catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}

function* watchAddUser() {
    yield takeEvery(ADD_USER, addUser)
}

export function* updateUser(action) {
    yield put({type: SHOW_SPINNER});
    const updatedUser = { ...action.payload.data };

    try {
        yield call(axios.put, `http://localhost:3001/users`, {user:updatedUser});
        yield put({ type: FETCH_USERS });
        yield put({type: HID_SPINNER});
    } catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}

function* watchUpdateUser() {
    yield takeEvery(UPDATE_USER, updateUser)
}

export function* deleteUser(action) {
    yield put({type: SHOW_SPINNER});
    try {
        yield call(axios.delete, `http://localhost:3001/users/${action.payload.id}`);
        yield put({ type: FETCH_USERS });
        yield put({type: HID_SPINNER});
    } catch (error) {
        yield put({type: HID_SPINNER});
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}

function* watchDeleteUser() {
    yield takeEvery(DELETE_USER, deleteUser)
}

export default function* usersSagas() {
    yield all([
        watchFetchUsers(),
        watchAddUser(),
        watchUpdateUser(),
        watchDeleteUser()
    ])
};
