import React, { Component } from "react";
import { connect } from 'react-redux';
import UserItem from '../../components/userItem';
import {fetchUsers, deleteUser} from './action'

class UserList extends Component {
    constructor(props) {
        super(props);
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onAdd = this.onAdd.bind(this);
    }

    componentWillMount(){
        if(!this.props.user)
            return this.props.history.push('/');
        if(!this.props.user.isAdmin)
            this.props.history.push('/chat');
    }

    componentDidMount() {
        this.props.fetchUsers();
    }

    onEdit(id) {
        this.props.history.push(`/user/${id}`);
    }

    onDelete(id) {
        this.props.deleteUser(id);
    }

    onAdd() {
        this.props.history.push('/user');
    }
    onChat(){
        this.props.history.push('/chat');
    }

    render() {
        return (
            <div className={"row"}>
                <div className={"list-group col-10"}>
                    {
                        this.props.users.map(user => {
                            return (
                                <UserItem
                                    key={user.id}
                                    id={user.id}
                                    name={user.user}
                                    onEdit={this.onEdit}
                                    onDelete={this.onDelete}
                                />
                            );
                        })
                    }
                </div>
                <div className={"col-1"}>
                    <button
                        className="btn btn-success"
                        onClick={this.onAdd}
                        style={{ margin: "5px" }}
                    >
                        Add user
                    </button>
                </div>
                <div className={"col-1"}>
                    <button
                        className="btn btn-success"
                        onClick={()=> this.onChat()}
                        style={{ margin: "5px" }}
                    >
                        Chat
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users,
        user: state.chat.user
    }
};

const mapDispatchToProps = {
    fetchUsers,
    deleteUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);