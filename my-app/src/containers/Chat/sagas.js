import { all, takeEvery, call, put } from 'redux-saga/effects';
import axios from 'axios';
import {SET_MESSAGES, SET_USER, DELETE_MESSAGE, SET_LIKE, SEND_MESSAGE} from "./actionTypes";
import {DISABLE_ERROR, ERROR_WITH_MESSAGE, HID_SPINNER, SHOW_SPINNER} from "../Login/actionTypes";
import delay from "../../helpers/delay";

export function* loadUsers(){
    yield put({type: SHOW_SPINNER});
    try {
        const messages = (yield call(axios.get, `http://localhost:3001/messages`)).data;
        yield put({ type: SET_MESSAGES, payload: {messages}});
        yield put({type: HID_SPINNER});
    } catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}
export function* deleteMessage(action){
    yield put({type: SHOW_SPINNER});
    try{
        yield call(fetch, `http://localhost:3001/messages/${action.payload.id}`,{method:"DELETE"})
        yield put({type: HID_SPINNER});
    }catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}
export function* setLike(action){
    yield put({type: SHOW_SPINNER});
    try{
        yield call(axios.put, `http://localhost:3001/messages/like`, {id: action.payload.id, user: action.payload.user})
        yield put({type: HID_SPINNER});
    }catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}
export function* sendMessage(action){
    yield put({type: SHOW_SPINNER});
    try{
        yield call(axios.post, `http://localhost:3001/messages`, {message: action.payload.message});
        yield put({type: HID_SPINNER});
    }catch(error){
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}
function* watchSetUser(){
    yield takeEvery(SET_USER, loadUsers);
}
function* watchDeleteMessage(){
    yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

function* watchSetLike(){
    yield takeEvery(SET_LIKE, setLike);
}


function* watchSendMessage(){
    yield takeEvery(SEND_MESSAGE, sendMessage);
}

export default function* chat() {
    yield all([
        watchSetUser(),
        watchDeleteMessage(),
        watchSetLike(),
        watchSendMessage()
    ])
};