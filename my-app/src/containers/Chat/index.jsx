import React from 'react';
import {Header} from '../../components/Header';
import MessagesList from '../../components/MessagesList';
import Send from '../../components/Send';
import {connect} from "react-redux";
import {showModal} from "../MessageEdit/action";
import {deleteMessage, setLike, sendMessage} from "./action";
import {Spinner} from "../../components/Spinner";

class Chat extends React.Component {

    componentWillMount() {
        if (!this.props.user)
            this.props.history.push('/');
    }

    changeMessageText = (id) => {
        this.props.showModal(id);
        this.props.history.push('/editMessage');
    };

    onEdit = () => {
        this.props.history.push('/users');
    };

    render() {
        if (!this.props.user)
            return null;
        if(this.props.messages && this.props.messages.length > 0)
        return (
            <div>
                {this.props.user && this.props.user.isAdmin ?
                    <button className={"btn btn-primary edit-wrp"} onClick={() => this.onEdit()}>edit
                        users</button> : null}
                <div className={'container'}>
                    <Header name={"myChat"} messages={this.props.messages}/>
                    <MessagesList messages={this.props.messages} user={this.props.user}
                                  deleteMessage={this.props.deleteMessage} setLike={this.props.setLike}
                                  changeMessageText={(id) => this.changeMessageText(id)}/>
                    <Send sendMessage={this.props.sendMessage} user={this.props.user}/>
                </div>
            </div>
        );
        return null;
    }
}


const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        user: state.chat.user
    };
};

const mapDispatchToProps = {
    showModal,
    deleteMessage,
    setLike,
    sendMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);