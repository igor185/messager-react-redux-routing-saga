import {DELETE_MESSAGE, SET_LIKE, SET_MESSAGES, SEND_MESSAGE} from "./actionTypes";

export const deleteMessage = (id) =>{
    return {
        type: DELETE_MESSAGE,
        payload:{
            id
        }
    }
};
export const setLike = (id, user) =>{
    return {
        type: SET_LIKE,
        payload:{
            id, user
        }
    }
};

export const setMessages = (messages) =>{
    return {
        type:SET_MESSAGES,
        payload:{
            messages
        }
    }
};

export const sendMessage = (message) =>{
    return{
        type:SEND_MESSAGE,
        payload:{
            message
        }
    }
};