import axios from 'axios';
import api from '../../config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_USER, FETCH_USER_SUCCESS } from "./actionTypes";
import {DISABLE_ERROR, ERROR_WITH_MESSAGE, HID_SPINNER, SHOW_SPINNER} from "../Login/actionTypes";
import delay from "../../helpers/delay";

export function* fetchUser(action) {
    try {
        const user = yield call(axios.get, `${api.url}/users/${action.payload.id}`);
        yield put({ type: FETCH_USER_SUCCESS, payload: { userData: user.data } });
    } catch (error) {
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}

function* watchFetchUser() {
    yield takeEvery(FETCH_USER, fetchUser)
}

export default function* userPageSagas() {
    yield all([
        watchFetchUser()
    ])
};
