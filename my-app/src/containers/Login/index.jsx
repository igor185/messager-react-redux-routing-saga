import React from 'react';
import {login, onChange} from "./action";
import connect from "react-redux/es/connect/connect";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user) {
            if (nextProps.user.isAdmin)
                return this.props.history.push('/users');
            this.props.history.push('/chat');
        }

    }

    render() {
        return (
                <form className={"container"}>
                    <div className={"form-group"}>
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Enter email" onChange={(e) => this.onChangeName(e)}/>
                    </div>
                    <div className={"form-group"}>
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1"
                               placeholder="Password"
                               onChange={(e) => this.onChangePassword(e)}/>
                    </div>
                    <button type="button" className="btn btn-primary"
                            onClick={() => this.props.login(this.state.name, this.state.password)}>Submit
                    </button>
                </form>
        )
    }

    onChangeName(e) {
        if(this.props.error)
            this.props.onChange();
        this.setState({name: e.target.value});
    }

    onChangePassword(e) {
        if(this.props.error)
            this.props.onChange();
        this.setState({password: e.target.value});
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.chat.user,
        error: state.login.errorMessage,
    }
};

const mapDispatchToProps = {
    login,
    onChange
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
