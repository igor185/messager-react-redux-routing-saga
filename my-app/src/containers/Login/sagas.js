import {SUBMIT_LOGIN, AUTH_ERROR, ERROR, SHOW_SPINNER, HID_SPINNER} from "./actionTypes";
import axios from 'axios';

import { all, takeEvery, call, put } from 'redux-saga/effects';
import {SET_USER} from "../Chat/actionTypes";

export function* auth(action){
    try {
        yield put({type: SHOW_SPINNER});
        const user = (yield call(axios.post, `http://localhost:3001/users/auth`, {user: action.payload.user, password: action.payload.password })).data;

        yield put({type: HID_SPINNER});

        localStorage.setItem('token', user.token);

        yield put({ type: SET_USER, payload: {user}});
    } catch (error) {
        yield put({type: HID_SPINNER});
        if(error.response && error.response.status === 403)
            yield put({ type: AUTH_ERROR});
        else
        yield put({ type: ERROR});
        console.log(error);
    }
}

function* watchAuth(){
    yield takeEvery(SUBMIT_LOGIN, auth);
}

export default function* login() {
    yield all([
        watchAuth()
    ])
};