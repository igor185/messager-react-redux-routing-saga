export const SUBMIT_LOGIN = "SUBMIT LOGIN";
export const FETCH_LOGIN = "FETCH LOGIN";
export const DISABLE_ERROR = "DISABLE_ERROR";
export const AUTH_ERROR = "AUTH_ERROR";
export const ERROR = "ERROR";
export const SHOW_SPINNER = "SHOW_SPINNER";
export const HID_SPINNER = "HID_SPINNER";
export const ERROR_WITH_MESSAGE = "ERROR_WITH_MESSAGE";
