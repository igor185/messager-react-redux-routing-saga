import {SUBMIT_LOGIN, DISABLE_ERROR} from "./actionTypes";

export const login = (user, password) => {
    return {
        type: SUBMIT_LOGIN,
        payload: {
            user, password
        }
    }
};

export const onChange = ()=>{
    return{
        type: DISABLE_ERROR
    }
};