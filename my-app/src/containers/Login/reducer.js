import {AUTH_ERROR, DISABLE_ERROR, ERROR, FETCH_LOGIN, SUBMIT_LOGIN, SHOW_SPINNER, HID_SPINNER, ERROR_WITH_MESSAGE} from "./actionTypes";

const initState = {
    errorMessage: '',
    isShownSpinner: false,
};
const AUTH_ERROR_MESSAGE = "Incorrect data";
const ERROR_MESSAGE = "Something went wrong";


export default function (state = initState, action) {
    switch (action.type) {
        case FETCH_LOGIN:
        case SUBMIT_LOGIN:
            return state;
        case DISABLE_ERROR:
            return {
                ...state,
                errorMessage: ""
            };
        case AUTH_ERROR:
            return {
                ...state,
                errorMessage: AUTH_ERROR_MESSAGE
            };
        case ERROR:
            return {
                ...state,
                errorMessage: ERROR_MESSAGE
            };
        case SHOW_SPINNER:
            return{
                ...state,
                isShownSpinner: true
            };
        case HID_SPINNER:
            return{
                ...state,
                isShownSpinner: false
            };
        case ERROR_WITH_MESSAGE:
            return{
                ...state,
                errorMessage: action.payload.message
            };
        default:
            return state;
    }
}