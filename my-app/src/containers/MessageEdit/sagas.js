import { all, takeEvery, call, put} from 'redux-saga/effects';
import {CHANGE_MESSAGE} from "./actionTypes";
import axios from "axios";
import {DISABLE_ERROR, ERROR_WITH_MESSAGE, HID_SPINNER, SHOW_SPINNER} from "../Login/actionTypes";
import delay from "../../helpers/delay";

export function* changeMessage(action){
    yield put({type: SHOW_SPINNER});
    try{
        yield call(axios.put, `http://localhost:3001/messages`, {id: action.payload.id, value: action.payload.value});
        yield put({type: HID_SPINNER});
    }catch(error){
        yield put({type:ERROR_WITH_MESSAGE, payload:{message:error.message}});
        console.log('fetchUsers error:', error.message);
        yield put({type: HID_SPINNER});
        yield delay(5000);
        yield put({type: DISABLE_ERROR});
    }
}

function* watchChangeMessage(){
    yield takeEvery(CHANGE_MESSAGE,changeMessage );
}

export default function* chat() {
    yield all([
        watchChangeMessage()
    ])
};