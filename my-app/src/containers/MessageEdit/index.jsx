import React from 'react'
import {connect} from "react-redux";
import findInArray from "../../helpers/findInArray.helper";
import {TextArea} from '../../components/TextArea';
import {changeMessage, hideModal} from './action';

class MessageEdit extends React.Component {

    value = '';

    componentWillMount() {
        if (!this.props.messageId)
            this.props.history.push('/chat');
    }

    getView = (value) => {
        this.value = value;
        return (
            <div className={"modal"} style={{display: 'block'}} tabIndex="-1" role="dialog">
                <div className={"modal-dialog"} role="document">
                    <div className={"modal-content"}>
                        <div className={"modal-header"}>
                            <h5 className={"modal-title"}>Edit message</h5>
                            <button type={"button"} className={"close"} data-dismiss={"modal"} aria-label={"Close"}
                                    onClick={() => this.onCancel()}>
                                <span aria-hidden={"true"}>&times;</span>
                            </button>
                        </div>
                        <div className={"modal-body"}>
                            <TextArea value={value} change={(value) => this.change(value)}/>
                        </div>
                        <div className={"modal-footer"}>
                            <button type={"button"} className={"btn btn-secondary"} data-dismiss={"modal"}
                                    onClick={() => this.onCancel()}>Cancel
                            </button>
                            <button type={"button"} className={"btn btn-primary"} onClick={() => this.onSave()}>Save
                                changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    };


    render() {
        const id = this.props.messageId;
        const index = findInArray(this.props.messages, id, 'id');
        if (index !== -1)
            return this.getView(this.props.messages[index].message);
        return null;
    }

    change = (value) => {
        this.value = value;
    };

    onCancel() {
        this.value = '';
        this.props.history.push('/chat');
    }

    onSave() {
        this.props.changeMessage(this.props.messageId, this.value);
        this.onCancel();
    }
}

const mapStateToProps = (state) => {
    return {
        messageId: state.modal.messageId,
        isShown: state.modal.isShown,
        messages: state.chat.messages
    };
};

const mapDispatchToProps = {
    hideModal,
    changeMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEdit);