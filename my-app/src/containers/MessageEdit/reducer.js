import {HIDE_MODAL, SHOW_MODAL} from "./actionTypes";

const initState = {
    messageId:'',
};

export default function(state = initState, action){
    switch (action.type) {
        case SHOW_MODAL:
            return{
                ...state,
                messageId: action.payload.messageId
            };
        case HIDE_MODAL:
            return{
                ...state,
                messageId: ''
            };
        default:
            return state;
    }
}