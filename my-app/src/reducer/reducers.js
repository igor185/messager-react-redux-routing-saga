import {combineReducers} from "redux";
import chat from '../containers/Chat/reducer';
import modal from '../containers/MessageEdit/reducer';
import login from '../containers/Login/reducer';
import users from '../containers/users/reducer';
import userPage from '../containers/usersEdit/reducer'

import {routerReducer} from "react-router-redux";

export const rootReducer = combineReducers({chat, modal, login, users, userPage, router: routerReducer});