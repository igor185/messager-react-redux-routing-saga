import { all } from 'redux-saga/effects';
import login from '../containers/Login/sagas';
import chat from '../containers/Chat/sagas';
import modal from '../containers/MessageEdit/sagas';
import users from '../containers/users/sagas';
import userPage from '../containers/usersEdit/sagas';

export default function* rootSaga() {
    yield all([
        login(),
        chat(),
        modal(),
        users(),
        userPage()
    ])
};