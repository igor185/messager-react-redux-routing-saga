import React from 'react';
import Chat from './containers/Chat/index';
import Modal from './containers/MessageEdit';
import {Route, Switch} from 'react-router-dom';
import UserList from './containers/users/index'
import UserPage from './containers/usersEdit/index';

import Login from "./containers/Login/index";
import {withRouter} from "react-router";
import {connect} from "react-redux";
import {Spinner} from "./components/Spinner";

import './app.css';


const App = (props) => (
    <div>
        <Switch>
            <Route path='/chat' component={Chat}/>
            <Route path='/editMessage' component={Modal}/>
            <Route exact path='/users' component={UserList}/>
            <Route exact path='/user' component={UserPage}/>
            <Route path='/user/:id' component={UserPage}/>
            <Route path='/' component={Login}/>
        </Switch>
        {
            props.isShownSpinner ?
                <div style={{position: "absolute", top: "0"}}>
                    <Spinner/>
                </div> : null
        }
        {props.errorMessage ?
            <div className={"alert alert-danger error-message-wrp"} role="alert">
                {props.errorMessage}
            </div>
            :
            null
        }
    </div>
);

const mapStateToProps = (state) => {
    return {
        isShownSpinner: state.login.isShownSpinner,
        errorMessage: state.login.errorMessage
    };
};


export default withRouter(connect(mapStateToProps)(App));
