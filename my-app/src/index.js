import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'
import {Provider} from "react-redux";
import store from './store'
import {Router, Route} from 'react-router-dom';


import { createBrowserHistory } from 'history';


ReactDOM.render(
    <Provider store={store}>
        <Router history={createBrowserHistory()}>
            <Route path="/" component={App} />
        </Router>
    </Provider>,
    document.getElementById('root')
);

