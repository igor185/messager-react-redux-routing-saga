import React from 'react';
import './index.css'
import {countUniqUsers} from "../../helpers/countUniqUsers";

export const Header = (props) => {

    const messages = [...(props.messages || [])];
    const amountUsers = countUniqUsers(messages);
    let lastMessTime;
    if (messages.length > 0)
        lastMessTime = new Date(messages[messages.length - 1].created_at);
    else
        lastMessTime = "unknown";
    const amountMessages = messages.length;

    return (
        <header className={'header border'}>
            <div>
                {props.name}
            </div>
            <div>
                {amountUsers} participants
            </div>
            <div>
                {amountMessages} messages
            </div>
            <div>
                last message {lastMessTime.toLocaleString()}
            </div>
        </header>
    );
};
