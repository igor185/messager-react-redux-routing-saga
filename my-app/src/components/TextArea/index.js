import React from 'react';

export const TextArea = ({value, change}) => (
    <textarea name="" id="" cols="50" rows="5" defaultValue={value} onChange={(e) => change(e.target.value)}/>
);

