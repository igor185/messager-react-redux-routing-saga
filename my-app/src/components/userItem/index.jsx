import React from "react";

const UserItem = (props) => {
    const {id, name} = props;
    return (
        <div className={"container list-group-item"}>
            <div className={"row"}>
                <div className={"col-8"}>
                        <span className={"badge badge-secondary float-left"}
                              style={{fontSize: "2em", margin: "2px"}}>{name}</span>
                </div>
                <div className={"col-4 btn-group"}>
                    <button className={"btn btn-outline-primary"} onClick={() => props.onEdit(id)}> Edit
                    </button>
                    <button className={"btn btn-outline-dark"} onClick={() => props.onDelete(id)}> Delete
                    </button>
                </div>
            </div>
        </div>
    );
};

export default UserItem;
