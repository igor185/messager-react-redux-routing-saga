import React from 'react'
import './style.css'

export const TimeLine = ({time}) => (
    <div className={'time-line-wrp'}>
        <span className={'time-line'}>{time}</span>
    </div>
);


