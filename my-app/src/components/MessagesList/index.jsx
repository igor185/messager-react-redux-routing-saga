import React from 'react';
import {Message} from '../Message/index';
import {TimeLine} from '../TimeLine/index';
import './style.css'

const MessagesList = (props) => {

    let now = new Date();
    let messages = [...props.messages];
    let messages_Time = [];
    let tempDate = new Date();
    for (let i = 0; i < messages.length; i++) {
        let j = new Date(messages[i].created_at);
        if (tempDate && tempDate.getDay() !== j.getDay()) {
            if (j.getDay() === now.getDay())
                messages_Time.push(<TimeLine time={'Today'} key={j.toLocaleString()}/>);
            else if (j.getDay() + 1 === now.getDay())
                messages_Time.push(<TimeLine time={'Yesterday'} key={j.toLocaleString()}/>);
            else
                messages_Time.push(<TimeLine time={j.toLocaleDateString().split(',')[0]}
                                             key={j.toLocaleString()}/>);
        }
        tempDate = j;
        let elem = messages[i];
        messages_Time.push(<Message key={elem.id} setLike={props.setLike}
                                    deleteMessage={() => props.deleteMessage(elem.id)}
                                    changeMessageText={props.changeMessageText} message={elem}
                                    user={props.user}
                                    showModal={() => props.showModal(elem.id)}/>)
    }

    return (
        <div className={"border messagesList"} id={"messagesList"} style={{height: "calc(100vh - 80px)"}}>
            {messages_Time}
        </div>
    )
};


export default MessagesList;
