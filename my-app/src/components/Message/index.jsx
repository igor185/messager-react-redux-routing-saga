import React from 'react';
import './style.css'
import {Avatar} from '../Avatar/index';
import {Icon} from '../Icon/index';
import api from '../../config/api';

export const Message = (props) => {
    const message = props.message;
    let selfPost, icons;
    if (message.user === props.user.user) {
        selfPost = 'self-right';
        icons = <div>
            <Icon src={api.delete_src} callback={() => props.deleteMessage(props.message.id)}/>
            <Icon src={api.rewrite_src} callback={() => props.changeMessageText(props.message.id)}/>
        </div>;
    } else {
        selfPost = 'self-left';
        icons = <div>
            <Icon src={api.like_src} callback={() => props.setLike(props.message.id, props.user.user)}/>
            {message.reactions ? message.reactions.length : 0}
        </div>;
    }

    return (
        <div className={`message-wrp ${selfPost}`}>
            <div className={'card mb-3'}>
                <div className={"row no-gutters"}>
                    <div className={"col-md-4 avatar-wrp"}>
                        <Avatar url={message.avatar}/>
                    </div>
                    <div className={"col-md-8"}>
                        <div className={"card-body"}>
                            <h5 className="card-title">{message.user}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{(new Date(message.created_at)).toLocaleTimeString()}</h6>
                            <div className={"card-text"}>
                                {message.message}
                            </div>
                            {icons}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

