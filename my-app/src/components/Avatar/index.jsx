import React from 'react';
import './style.css';
import api from '../../config/api';



export const Avatar =  ({url}) => {
        const src = url ? url : api.default_avatar;
        return (<img src={src} className={'card-img avatar-img'} alt="avatar"/>)
};

