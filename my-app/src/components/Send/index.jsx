import React, {useState} from 'react';

const Send = (props) => {
    const [value, setValue] = useState('');

    let sendMessage = () => {
        const message = {
            id: `f${(+new Date()).toString(16)}`,
            created_at: (new Date()).toJSON(),
            message: value,
            marked_read: false,
            ...props.user
        };

        setValue('');
        props.sendMessage(message);
    };

    return (
        <div className={"border"} style={{height: "40px"}}>
            <div className={"input-group"}>
                <input type="text" className={"form-control"} placeholder={"send message"} value={value}
                       onChange={e => setValue(e.target.value)}/>
                <div className={"input-group-append"}>
                    <input type={'button'} className={"input-group-text btn btn-primary"} value={"send"}
                           onClick={() => {
                               setValue('');
                               sendMessage();
                           }}/>
                </div>
            </div>
        </div>
    )
};

export default Send;
