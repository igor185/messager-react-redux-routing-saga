import React from 'react'
import './style.css';

export const Icon = (props) => {
    return (
        <img src={props.src} className={'icon'} alt="icon"
             onClick={() => props.callback ? props.callback() : null}/>
    )
};