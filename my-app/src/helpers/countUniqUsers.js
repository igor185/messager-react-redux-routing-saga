export const countUniqUsers = (messages) => {
    const users = new Set();
    messages.forEach(elem => users.add(elem.user));
    return users.size;
};